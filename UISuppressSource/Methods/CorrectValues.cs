﻿using GrozaSModelsDBLib;

namespace UISuppressSource
{
    public class CorrectValues
    {
        public static void IsCorrectScanSpeed(TableSuppressSource source)
        {
            if (source.ScanSpeed < 1) { source.ScanSpeed = 1; }
            if (source.ScanSpeed > 255) { source.ScanSpeed = 255; }
        }

        public static void IsCorrectDeviation(TableSuppressSource source)
        {
            if (source.Deviation < 500) { source.Deviation = 500; }
            if (source.Deviation > 127500) { source.Deviation = 127500; }
        }


        //public static void IsCorrectBand(DronePropertiesModel properties)
        //{
        //    if (properties.Band < 1) { properties.Band = 1; }
        //    if (properties.Band > 100) { properties.Band = 100; }
        //}


        //public static void IsCorrectDistance(DronePropertiesModel properties)
        //{
        //    //if (properties.DistanceOwn < 0) { properties.DistanceOwn = 0; }
        //    //if (properties.DistanceOwn > 50000) { properties.DistanceOwn = 50000; }
        //}

        //public static void IsCorrectType(DronePropertiesModel properties)
        //{
        //    if (properties.Type == null) { properties.Type = "No Image"; }
        //}
    }
}
