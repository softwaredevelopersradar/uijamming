﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using GrozaSModelsDBLib;

namespace UISuppressSource
{
    public class JammingViewModel : DependencyObject, INotifyPropertyChanged
    {

        private TableSuppressSource _range100_500 = new TableSuppressSource() { InputType = TypeInput.Manual };

        public TableSuppressSource Range100_500
        {
            get { return _range100_500; }
            set
            {
                if (_range100_500 == value) return;

                _range100_500 = value;
                OnPropertyChanged();
            }
        }


        private TableSuppressSource _range500_2500 = new TableSuppressSource() { InputType = TypeInput.Manual };

        public TableSuppressSource Range500_2500
        {
            get { return _range500_2500; }
            set
            {
                if (_range500_2500 == value) return;

                _range500_2500 = value;
                OnPropertyChanged();
            }
        }



        private TableSuppressSource _range2500_6000 = new TableSuppressSource() { InputType = TypeInput.Manual };

        public TableSuppressSource Range2500_6000
        {
            get { return _range2500_6000; }
            set
            {
                if (_range2500_6000 == value) return;

                _range2500_6000 = value;
                OnPropertyChanged();
            }
        }


        private TableSuppressGnss _rangeGps = new TableSuppressGnss() {Type = TypeGNSS.Gps };

        public TableSuppressGnss RangeGps
        {
            get { return _rangeGps; }
            set
            {
                if (_rangeGps == value) return;

                _rangeGps = value;
                OnPropertyChanged();
            }
        }


        private TableSuppressGnss _rangeGlonass = new TableSuppressGnss() { Type = TypeGNSS.Glonass };

        public TableSuppressGnss RangeGlonass
        {
            get { return _rangeGlonass; }
            set
            {
                if (_rangeGlonass == value) return;

                _rangeGlonass = value;
                OnPropertyChanged();
            }
        }


        private TableSuppressGnss _rangeBeidow = new TableSuppressGnss() { Type = TypeGNSS.Beidou };

        public TableSuppressGnss RangeBeidou
        {
            get { return _rangeBeidow; }
            set
            {
                if (_rangeBeidow == value) return;

                _rangeBeidow = value;
                OnPropertyChanged();
            }
        }


        private TableSuppressGnss _rangeGalileo = new TableSuppressGnss() { Type = TypeGNSS.Galileo };

        public TableSuppressGnss RangeGalileo
        {
            get { return _rangeGalileo; }
            set
            {
                if (_rangeGalileo == value) return;

                _rangeGalileo = value;
                OnPropertyChanged();
            }
        }


        private State _stateRange100_500 = new State();

        public State StateRange100_500
        {
            get { return _stateRange100_500; }
            set
            {
                if (_stateRange100_500 == value) return;

                _stateRange100_500 = value;
                OnPropertyChanged();
            }
        }

        //private State _stateRange500_2500 = new State() { Current = 50.86f, Temperature=32.1f, Power = StateMode.Ok, Radiation = StateMode.Error, Synthesizer = StateMode.Ok };
        private State _stateRange500_2500 = new State();

        public State StateRange500_2500
        {
            get { return _stateRange500_2500; }
            set
            {
                if (_stateRange500_2500 == value) return;

                _stateRange500_2500 = value;
                OnPropertyChanged();
            }
        }

        private State _stateRange2500_6000 = new State();

        public State StateRange2500_6000
        {
            get { return _stateRange2500_6000; }
            set
            {
                if (_stateRange2500_6000 == value) return;

                _stateRange2500_6000 = value;
                OnPropertyChanged();
            }
        }

        private State _stateGNSS = new State();

        public State StateGNSS
        {
            get { return _stateGNSS; }
            set
            {
                if (_stateGNSS == value) return;

                _stateGNSS = value;
                OnPropertyChanged();
            }
        }

        private State _stateSpoof = new State();

        public State StateSpoof
        {
            get { return _stateSpoof; }
            set
            {
                if (_stateSpoof == value) return;

                _stateSpoof = value;
                OnPropertyChanged();
            }
        }


        //private int _ampNum ;

        //public int AmpNum
        //{
        //    get { return _ampNum; }
        //    set
        //    {
        //        if (_ampNum == value) return;

        //        _ampNum = value;
        //        OnPropertyChanged();
        //    }
        //}

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
    }
}
