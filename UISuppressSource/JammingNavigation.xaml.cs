﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using GrozaSModelsDBLib;

namespace UISuppressSource
{
    /// <summary>
    /// Interaction logic for JammingNavigation.xaml
    /// </summary>
    public partial class JammingNavigation : UserControl
    {
        public JammingNavigation()
        {
            InitializeComponent();
        }

        public event EventHandler<TableSuppressGnss> StateChanged;
        public event EventHandler<(bool, TableSuppressGnss)> SectionActivated;

        #region DP
        public string Range
        {
            get { return (string)GetValue(RangeProperty); }
            set { SetValue(RangeProperty, value); }
        }

        public static readonly DependencyProperty RangeProperty =
            DependencyProperty.Register("Range", typeof(string),
            typeof(JammingNavigation), new FrameworkPropertyMetadata(" "));


        public string L1Content
        {
            get { return (string)GetValue(L1ContentProperty); }
            set { SetValue(L1ContentProperty, value); }
        }

        public static readonly DependencyProperty L1ContentProperty =
            DependencyProperty.Register("L1Content", typeof(string),
            typeof(JammingNavigation), new FrameworkPropertyMetadata("L1"));


        public string L2Content
        {
            get { return (string)GetValue(L2ContentProperty); }
            set { SetValue(L2ContentProperty, value); }
        }

        public static readonly DependencyProperty L2ContentProperty =
            DependencyProperty.Register("L2Content", typeof(string),
            typeof(JammingNavigation), new FrameworkPropertyMetadata("L2"));

        public string L3Content
        {
            get { return (string)GetValue(L3ContentProperty); }
            set { SetValue(L3ContentProperty, value); }
        }

        public static readonly DependencyProperty L3ContentProperty =
            DependencyProperty.Register("L3Content", typeof(string),
            typeof(JammingNavigation), new FrameworkPropertyMetadata("L3"));



        public TableSuppressGnss Source
        {
            get { return (TableSuppressGnss)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }

        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(TableSuppressGnss),
            typeof(JammingNavigation), new FrameworkPropertyMetadata(new TableSuppressGnss()));


        public bool IsActivated
        {
            get { return (bool)GetValue(IsActivatedProperty); }
            set { SetValue(IsActivatedProperty, value); }
        }

        public static readonly DependencyProperty IsActivatedProperty =
            DependencyProperty.Register("IsActivated", typeof(bool),
            typeof(JammingNavigation), new FrameworkPropertyMetadata(false));


        public ExternalMode Mode
        {
            get { return (ExternalMode)GetValue(ModeProperty); }
            set { SetValue(ModeProperty, value); }
        }

        public static readonly DependencyProperty ModeProperty =
            DependencyProperty.Register("Mode", typeof(ExternalMode),
            typeof(JammingNavigation), new FrameworkPropertyMetadata(ExternalMode.Stop));


        public LettersVisibility LettersVisibility
        {
            get { return (LettersVisibility) GetValue(LettersVisibilityProperty); }
            set { SetValue(LettersVisibilityProperty, value);}
        }

        public static readonly DependencyProperty LettersVisibilityProperty =
            DependencyProperty.Register("LettersVisibility", typeof(LettersVisibility),
            typeof(JammingNavigation), new FrameworkPropertyMetadata(new LettersVisibility()));
        #endregion


        private void Polygon_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            IsActivated = !IsActivated;
            SectionActivated?.Invoke(this, (IsActivated, Source.Clone()));
        }

        private void btnLetter_Click(object sender, RoutedEventArgs e)
        {
            var newRec = Source.Clone();
            newRec.L1 = (bool)btnL1.IsChecked;
            newRec.L2 = (bool)btnL2.IsChecked;
            StateChanged?.Invoke(this, newRec);
        }
    }
}
