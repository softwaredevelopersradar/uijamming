﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace UISuppressSource
{
    /// <summary>
    /// Interaction logic for JammingStateHorizontal.xaml
    /// </summary>
    public partial class JammingStateHorizontal : UserControl
    {
        public event EventHandler<AntennaMode> AntennaSelected;

        public JammingStateHorizontal()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Состояние передатчика
        /// </summary>
        public State Indication
        {
            get { return (State)GetValue(IndicationProperty); }
            set { SetValue(IndicationProperty, value); }
        }

        public static readonly DependencyProperty IndicationProperty =
            DependencyProperty.Register("Indication", typeof(State),
                typeof(JammingStateHorizontal), new FrameworkPropertyMetadata(new State()));


        /// <summary>
        /// Наличие антенн 
        /// </summary>
        public StateAntenna Availability
        {
            get { return (StateAntenna)GetValue(AvailabilityProperty); }
            set { SetValue(AvailabilityProperty, value); }
        }

        public static readonly DependencyProperty AvailabilityProperty =
            DependencyProperty.Register("Availability", typeof(StateAntenna),
                typeof(JammingStateHorizontal), new FrameworkPropertyMetadata(new StateAntenna(){Log = true, Omni = false}));


        /// <summary>
        /// Выбранная антенна
        /// </summary>
        public StateAntenna ActiveAntenna
        {
            get { return (StateAntenna)GetValue(ActiveAntennaProperty); }
            set { SetValue(ActiveAntennaProperty, value); }
        }

        public static readonly DependencyProperty ActiveAntennaProperty =
            DependencyProperty.Register("ActiveAntenna", typeof(StateAntenna),
                typeof(JammingStateHorizontal), new FrameworkPropertyMetadata(new StateAntenna()));


        private void btnAntenna_Click(object sender, RoutedEventArgs e)
        {
            if (Availability == null)
                return;

            if (!Availability.Omni || !Availability.Log)
                return;

            AntennaMode mode = AntennaMode.Log;
            if ((sender as Button).Name == "btnOmni")
                mode = AntennaMode.OMNI;

            AntennaSelected?.Invoke(this, mode);
        }
    }
}
