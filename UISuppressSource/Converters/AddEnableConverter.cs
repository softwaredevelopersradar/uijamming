﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace UISuppressSource
{
    public class AddEnableConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((EditMode)value == EditMode.Change)
                return true;
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool)value == true)
                return EditMode.Change;
            return EditMode.Add;
        }
    }
}
