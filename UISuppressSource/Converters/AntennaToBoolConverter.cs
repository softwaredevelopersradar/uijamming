﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace UISuppressSource
{
    public class AntennaToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var mode = (AntennaMode)value;
            var state = new StateAntenna();
            switch (mode)
            {
                case AntennaMode.Log:
                    state.Log = true;
                    break;
                case AntennaMode.OMNI:
                    state.Omni = true;
                    break;
                default:
                    break;
            }
            return state;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
