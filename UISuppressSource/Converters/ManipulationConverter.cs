﻿using System;
using System.Globalization;
using System.Windows.Data;
using GrozaSModelsDBLib;
using System.Collections.Generic;

namespace UISuppressSource
{
    public class ManipulationConverter: IValueConverter
    {
        
        //public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        //{
        //    //var record = (KeyValuePair<byte, string>)value;
        //    if (!ManipulationTypes.Keys.Contains(num))
        //        return "-";

        //    var record = ManipulationTypes[value];
        //    return record;
        //}

        //public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        //{
        //    return (ManipulationTypes)value;
        //}

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                var t = (byte)value;
                return ManipulationTypes.GetType(t);
            }
            catch 
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var s = (string)value;
            return ManipulationTypes.GetNum(s);
        }
    }
}
