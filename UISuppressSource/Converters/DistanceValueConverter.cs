﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace UISuppressSource
{
    public class DistanceValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (float)value / 1000;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (float)value * 1000;
        }
    }
}
