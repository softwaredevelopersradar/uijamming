﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows;
using GrozaSModelsDBLib;

namespace UISuppressSource
{
    public class InputTypeToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((TypeInput)value == TypeInput.Auto)
                return Visibility.Visible;
            return Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((Visibility)value == Visibility.Visible)
                return TypeInput.Auto;
            return TypeInput.Manual;
        }
    }
}
