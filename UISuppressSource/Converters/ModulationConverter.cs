﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using GrozaSModelsDBLib;

namespace UISuppressSource
{ 
    public class ModulationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //byte bRole = (byte)value;
            //return (byte)value;
            
            switch((Modulation)value)
            {
                case Modulation.KFM:
                    return (byte)1;
                case Modulation.LCM:
                    return (byte)2;
                //case Modulation.LCM_2:
                //    return (byte)3;
                default:
                    return (byte)0;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (System.Convert.ToByte(value))
            {
                case (byte)1:
                    return Modulation.KFM;
                case (byte)2:
                    return Modulation.LCM;
                //case (byte)3:
                //    return Modulation.LCM_2;
                default:
                    return Modulation.None;
            }
            //return (Modulation)System.Convert.ToByte(value);
        }
    }
}
