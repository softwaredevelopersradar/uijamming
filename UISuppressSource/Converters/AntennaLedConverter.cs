﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace UISuppressSource
{
    public class AntennaLedConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var mode = (bool)value;

            if(mode) return Brushes.Aqua;
            return new BrushConverter().ConvertFromString("#666666");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
