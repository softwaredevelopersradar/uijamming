﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace UISuppressSource
{
    public class CoordConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((double)value == 0)
                return "  ";
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((string)value == " ")
                return 0;
            return (double)value;
        }
    }
}
