﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Collections.Generic;

namespace UISuppressSource
{
    public class ManipulationConverter2 : IValueConverter
    {
        private List<string> collection = new List<string>()
                                              {
                                                  "-",
                                                  "12.5",
                                                  "10",
                                                  "7.5",
                                                  "5",
                                                  "2",
                                                  "1",
                                                  "0.2",
                                                  "0.1",
                                                  "0.05",
                                                  "0.025",
                                                  "0.0125",
                                                  "0.0025"
                                              };
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                var t = (byte)value;
                return collection[t];
            }
            catch
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
