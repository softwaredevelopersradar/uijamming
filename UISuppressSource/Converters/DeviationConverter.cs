﻿namespace UISuppressSource
{
    using System.Globalization;
    using System.Windows.Data;
    using System;

    public class DeviationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double bTemp = (int)value * 2d / 1000d;
            return bTemp;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double value1 = System.Convert.ToDouble(value);
            double bTemp = value1 / 2d * 1000d;
            return (int)bTemp;
        }
    }
}
