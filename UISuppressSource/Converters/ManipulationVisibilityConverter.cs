﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using GrozaSModelsDBLib;

namespace UISuppressSource
{
    public class ManipulationVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Modulation modulation = (Modulation)value;

            if(modulation == Modulation.KFM)
                return Visibility.Visible;

            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
