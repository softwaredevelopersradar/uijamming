﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace UISuppressSource
{
    public class ByteToTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var t = (int)value;

            if (t == 0)
                return String.Empty;

            return t.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
