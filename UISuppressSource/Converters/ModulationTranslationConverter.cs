﻿using System;
using System.Collections.Generic;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using GrozaSModelsDBLib;

namespace UISuppressSource
{
    public class ModulationTranslationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var sModulation = string.Empty;

            switch ((Modulation)value)
            {
                case Modulation.None:
                    sModulation = SMeaning.meaningNone; 
                    break; 
                
                case Modulation.KFM:
                    sModulation = SMeaning.meaningKFM; 
                    break;

                case Modulation.LCM:
                    sModulation = SMeaning.meaningLCM;
                    break;

                default:
                    return Binding.DoNothing;
            }

            return sModulation;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
