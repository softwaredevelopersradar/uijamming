﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using GrozaSModelsDBLib;

namespace UISuppressSource
{
    public class IndicatorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var mode = (TypeInput)value;

            if (mode == TypeInput.Manual)
                return Brushes.Transparent;
            else
                return new BrushConverter().ConvertFromString("#FF6347");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
