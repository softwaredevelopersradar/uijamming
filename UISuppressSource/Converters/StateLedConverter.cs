﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace UISuppressSource
{
    public class StateLedConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var mode = (StateMode)value;

            switch(mode)
            {
                case StateMode.Ok:
                    return Brushes.Aqua;
                case StateMode.Error:
                    return new BrushConverter().ConvertFromString("#FF6347");
                default:
                    return new BrushConverter().ConvertFromString("#666666");
            }
        }
        
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }

    
}
