﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace UISuppressSource
{
    public class ColorConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
        {
            var isActivated = (bool)value[0];
            var mode = (ExternalMode)value[1];

            if (!isActivated)
                return Brushes.Transparent;

            if (mode == ExternalMode.Jamming)
                    return new BrushConverter().ConvertFromString("#FF6347");

            return Brushes.Aqua;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
