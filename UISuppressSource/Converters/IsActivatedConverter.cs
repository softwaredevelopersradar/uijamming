﻿using System;
using System.Globalization;
using System.Windows.Data;
using GrozaSModelsDBLib;

namespace UISuppressSource
{
    public class IsActivatedConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var gnss = (TableSuppressGnss)value;

            if (gnss.L1 == false && gnss.L2 == false && gnss.L5 == false)
                return false;
            
            return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
