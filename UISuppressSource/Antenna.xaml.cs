﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace UISuppressSource
{
    /// <summary>
    /// Логика взаимодействия для Antenna.xaml
    /// </summary>
    public partial class Antenna : UserControl
    {
        public event EventHandler<AntennaMode> AntennaSelected;

        public Antenna()
        {
            InitializeComponent();
        }


        public StateAntenna Indication
        {
            get { return (StateAntenna)GetValue(IndicationProperty); }
            set { SetValue(IndicationProperty, value); }
        }

        public static readonly DependencyProperty IndicationProperty =
            DependencyProperty.Register("Indication", typeof(StateAntenna),
            typeof(Antenna), new FrameworkPropertyMetadata(new StateAntenna()));


        private void btnAntenna_Click(object sender, RoutedEventArgs e)
        {
            AntennaMode mode = AntennaMode.Log;
            if ((sender as Button).Name == "btnOmni") 
                mode = AntennaMode.OMNI;

            AntennaSelected?.Invoke(this, mode);
        }
    }
}
