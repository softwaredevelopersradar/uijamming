﻿
using System.Windows;
using System.Windows.Controls;

namespace UISuppressSource
{
    /// <summary>
    /// Логика взаимодействия для JammingState.xaml
    /// </summary>
    public partial class JammingState : UserControl
    {
        public JammingState()
        {
            InitializeComponent();
        }

        public State Indication
        {
            get { return (State)GetValue(IndicationProperty); }
            set { SetValue(IndicationProperty, value); }
        }

        public static readonly DependencyProperty IndicationProperty =
            DependencyProperty.Register("Indication", typeof(State),
            typeof(JammingState), new FrameworkPropertyMetadata(new State()));

        

    }
}
