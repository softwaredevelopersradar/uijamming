﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace UISuppressSource
{
    /// <summary>
    /// Логика взаимодействия для JammingNavigationAll.xaml
    /// </summary>
    public partial class JammingNavigationAll : UserControl
    {
        public JammingNavigationAll()
        {
            InitializeComponent();
        }

        //public event EventHandler<List<TableSuppressGnss>> GnssStateChanged;
        public event EventHandler<(bool, List<TableSuppressGnss>)> SectionActivated;

        #region DP
        public string Range
        {
            get { return (string)GetValue(RangeProperty); }
            set { SetValue(RangeProperty, value); }
        }

        public static readonly DependencyProperty RangeProperty =
            DependencyProperty.Register("Range", typeof(string),
            typeof(JammingNavigationAll), new FrameworkPropertyMetadata(" "));


        public TableSuppressGnss Gps
        {
            get { return (TableSuppressGnss)GetValue(GpsProperty); }
            set { SetValue(GpsProperty, value); }
        }

        public static readonly DependencyProperty GpsProperty =
            DependencyProperty.Register("Gps", typeof(TableSuppressGnss),
            typeof(JammingNavigationAll), new FrameworkPropertyMetadata(new TableSuppressGnss() { Type = TypeGNSS.Gps }));

        public TableSuppressGnss Glonass
        {
            get { return (TableSuppressGnss)GetValue(GlonassProperty); }
            set { SetValue(GlonassProperty, value); }
        }

        public static readonly DependencyProperty GlonassProperty =
            DependencyProperty.Register("Glonass", typeof(TableSuppressGnss),
            typeof(JammingNavigationAll), new FrameworkPropertyMetadata(new TableSuppressGnss() { Type = TypeGNSS.Glonass }));

        public TableSuppressGnss Beidou
        {
            get { return (TableSuppressGnss)GetValue(BeidouProperty); }
            set { SetValue(BeidouProperty, value); }
        }

        public static readonly DependencyProperty BeidouProperty =
            DependencyProperty.Register("Beidou", typeof(TableSuppressGnss),
            typeof(JammingNavigationAll), new FrameworkPropertyMetadata(new TableSuppressGnss() { Type = TypeGNSS.Beidou }));


        public bool IsActivated
        {
            get { return (bool)GetValue(IsActivatedProperty); }
            set { SetValue(IsActivatedProperty, value); }
        }

        public static readonly DependencyProperty IsActivatedProperty =
            DependencyProperty.Register("IsActivated", typeof(bool),
            typeof(JammingNavigationAll), new FrameworkPropertyMetadata(false));


        public ExternalMode Mode
        {
            get { return (ExternalMode)GetValue(ModeProperty); }
            set { SetValue(ModeProperty, value); }
        }

        public static readonly DependencyProperty ModeProperty =
            DependencyProperty.Register("Mode", typeof(ExternalMode),
            typeof(JammingNavigationAll), new FrameworkPropertyMetadata(ExternalMode.Stop));


        public bool IsBeidouEnabled
        {
            get { return (bool)GetValue(IsBeidouEnabledProperty); }
            set { SetValue(IsBeidouEnabledProperty, value); }
        }

        public static readonly DependencyProperty IsBeidouEnabledProperty =
            DependencyProperty.Register("IsBeidouEnabled", typeof(bool),
            typeof(JammingNavigationAll), new FrameworkPropertyMetadata(true));

        public bool IsGpsEnabled
        {
            get { return (bool)GetValue(IsGpsEnabledProperty); }
            set { SetValue(IsGpsEnabledProperty, value); }
        }

        public static readonly DependencyProperty IsGpsEnabledProperty =
            DependencyProperty.Register("IsGpsEnabled", typeof(bool),
                typeof(JammingNavigationAll), new FrameworkPropertyMetadata(true));
        #endregion


        private void Polygon_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!IsGpsEnabled && !IsActivated && (!CheckGnssEmpty(Gps)|| !CheckGnssEmpty(Beidou)))
            {
                return;
            }

            if (!IsActivated && CheckAllGnssEmpty())
            { return; }

            //ChangeActivationState();
            SectionActivated?.Invoke(this, (!IsActivated, new List<TableSuppressGnss>() { Gps.Clone(), Glonass.Clone(), Beidou.Clone() }));
        }

        private void ChangeActivationState()
        {
            IsActivated = !IsActivated;
            SectionActivated?.Invoke(this, (IsActivated, new List<TableSuppressGnss>() { Gps.Clone(), Glonass.Clone(), Beidou.Clone() }));
        }


        private bool CheckGnssEmpty(TableSuppressGnss gnss)
        {
            return !(gnss.L1 || gnss.L2 || gnss.L5);
        }

        private bool CheckAllGnssEmpty()
        {
            return CheckGnssEmpty(Gps) && CheckGnssEmpty(Glonass) && CheckGnssEmpty(Beidou);
        }

        //private void btnGnss_Click(object sender, RoutedEventArgs e)
        //{
        //    //if ((sender as ToggleButton).Name != "btnBeidouL1" && (sender as ToggleButton).Name != "btnBeidouL2")
        //    //{
        //    //    return;
        //    //}
        //    var record = Beidou.Clone();
        //    //record.L1 = (bool)btnBeidouL1.IsChecked;
        //    //record.L2 = (bool)btnBeidouL2.IsChecked;

        //    //Gps.L1 = record.L1;
        //    //btnGpsL1.IsEnabled = !record.L1;
        //    //Glonass.L1 = record.L1;
        //    //btnGlonassL1.IsEnabled = !record.L1;

        //    Gps.L2 = record.L2;
        //    btnGpsL2.IsEnabled = !record.L2;
        //    Glonass.L2 = record.L2;
        //    btnGlonassL2.IsEnabled = !record.L2;


        //    GnssStateChanged?.Invoke(this, record);
        //    GnssStateChanged?.Invoke(this, Gps.Clone());
        //    GnssStateChanged?.Invoke(this, Glonass.Clone());
        //}

        private void btnGps_Click(object sender, RoutedEventArgs e)
        {
            if (!IsGpsEnabled && IsActivated && !CheckGnssEmpty(Gps))
            {
                Gps.L1 = false;
                Gps.L2 = false;
                //IsActivated = false;
                return;
            }
            //GnssStateChanged?.Invoke(this, new List<TableSuppressGnss>() { Gps.Clone(), Glonass.Clone(), Beidou.Clone() } );

            if (IsActivated && CheckAllGnssEmpty())
            { ChangeActivationState(); return; }
            
            SectionActivated?.Invoke(this, (IsActivated, new List<TableSuppressGnss>() { Gps.Clone(), Glonass.Clone(), Beidou.Clone() }));
        }

        private void btnGlonass_Click(object sender, RoutedEventArgs e)
        {
            //GnssStateChanged?.Invoke(this, new List<TableSuppressGnss>() { Gps.Clone(), Glonass.Clone(), Beidou.Clone() });
            

            if (IsActivated && CheckAllGnssEmpty())
            { ChangeActivationState(); return; }
            
            SectionActivated?.Invoke(this, (IsActivated, new List<TableSuppressGnss>() { Gps.Clone(), Glonass.Clone(), Beidou.Clone() }));
        }

        private void btnGnss1_Click(object sender, RoutedEventArgs e)
        {
            var record = Beidou.Clone();
            if (!IsGpsEnabled && IsActivated && !CheckGnssEmpty(Beidou))
            {
                Beidou.L1 = false;
                Beidou.L2 = false;
                return;
            }

            Gps.L1 = record.L1;
            btnGpsL1.IsEnabled = !record.L1;
            Glonass.L1 = record.L1;
            btnGlonassL1.IsEnabled = !record.L1;

            //GnssStateChanged?.Invoke(this, new List<TableSuppressGnss>() { Gps.Clone(), Glonass.Clone(), record } );


            if (IsActivated && CheckAllGnssEmpty())
            { ChangeActivationState(); return; }
            
            SectionActivated?.Invoke(this, (IsActivated, new List<TableSuppressGnss>() { Gps.Clone(), Glonass.Clone(), Beidou.Clone() }));
        }

        private void btnGnss2_Click(object sender, RoutedEventArgs e)
        {
            var record = Beidou.Clone();
            if (!IsGpsEnabled && IsActivated && !CheckGnssEmpty(Beidou))
            {
                Beidou.L1 = false;
                Beidou.L2 = false;
                return;
            }

            Gps.L2 = record.L2;
            btnGpsL2.IsEnabled = !record.L2;
            Glonass.L2 = record.L2;
            btnGlonassL2.IsEnabled = !record.L2;

            //GnssStateChanged?.Invoke(this, new List<TableSuppressGnss>() { Gps.Clone(), Glonass.Clone(), record });
            

            if (IsActivated && CheckAllGnssEmpty())
            { ChangeActivationState(); return; }
            
            SectionActivated?.Invoke(this, (IsActivated, new List<TableSuppressGnss>() { Gps.Clone(), Glonass.Clone(), Beidou.Clone() }));
        }
    }
}
