﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System;
using GrozaSModelsDBLib;

namespace UISuppressSource
{
    /// <summary>
    /// Interaction logic for JammingFrequency.xaml
    /// </summary>
    public partial class JammingFrequency : UserControl
    {
        public JammingFrequency()
        {
            InitializeComponent();
        }

        public PropertyView propertyWindow;

        #region Events
        public event EventHandler<TableSuppressSource> OnChangeRecord;
        public event EventHandler OnClearRecord;

        public event EventHandler<bool> SectionActivated;
        // Открылось окно с PropertyGrid
        public event EventHandler<PropertyView> OnIsWindowPropertyOpen;
        #endregion



        #region DP
        public string Range
        {
            get { return (string)GetValue(RangeProperty); }
            set { SetValue(RangeProperty, value); }
        }

        public static readonly DependencyProperty RangeProperty =
            DependencyProperty.Register("Range", typeof(string),
            typeof(JammingFrequency), new FrameworkPropertyMetadata(" "));


        public TableSuppressSource Source
        {
            get { return (TableSuppressSource)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }

        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(TableSuppressSource),
            typeof(JammingFrequency), new FrameworkPropertyMetadata(new TableSuppressSource() {InputType  = TypeInput.Manual}));
        

        public bool IsActivated
        {
            get { return (bool)GetValue(IsActivatedProperty); }
            set { SetValue(IsActivatedProperty, value); }
        }

        public static readonly DependencyProperty IsActivatedProperty =
            DependencyProperty.Register("IsActivated", typeof(bool),
            typeof(JammingFrequency));


        public ExternalMode Mode
        {
            get { return (ExternalMode)GetValue(ModeProperty); }
            set { SetValue(ModeProperty, value); }
        }

        public static readonly DependencyProperty ModeProperty =
            DependencyProperty.Register("Mode", typeof(ExternalMode),
            typeof(JammingFrequency), new FrameworkPropertyMetadata(ExternalMode.Stop));/*, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault*/

        

        #endregion




        #region Buttons

        private void Polygon_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!IsActivated && Source.FrequencyMHz == 0)
            { return;}

            IsActivated = !IsActivated;
            SectionActivated?.Invoke(this, IsActivated);
        }


        private void BtnCallContextMenu_Click(object sender, RoutedEventArgs e)
        {
            ContextMenu cm = this.FindResource("cmButton") as ContextMenu;
            cm.PlacementTarget = sender as Button;
            cm.IsOpen = true;
        }


        private void UpdateItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Source == null)
                    { return; }
                
                propertyWindow = new PropertyView(MyGroupBox.Header.ToString(), Source);
                OnIsWindowPropertyOpen?.Invoke(this, propertyWindow);

                if (propertyWindow.ShowDialog() == true && !Source.EqualTo(propertyWindow.DroneProperties.General))
                {
                    OnChangeRecord?.Invoke(this, propertyWindow.DroneProperties.General);
                }
            }
            catch (Exception ex)
            { }
        }


        private void ClearItem_Click(object sender, RoutedEventArgs e)
        {
            OnClearRecord?.Invoke(this, null);
        }

        #endregion
    }
}
