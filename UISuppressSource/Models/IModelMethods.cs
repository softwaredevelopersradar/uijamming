﻿using System;

namespace UISuppressSource
{
    public interface IModelMethods<T> where T : class
    {
        bool EqualTo(T model);

        T Clone();

        void Update(T model);
    }
}
