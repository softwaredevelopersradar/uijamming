﻿

namespace UISuppressSource
{
    public enum ExternalMode : byte
    {
        Stop,
        RadioIntelegence,
        Jamming
    }


    public enum EditMode
    {
        Add,
        Change
    }


    public struct LettersVisibility
    {
        public bool L1;
        public bool L2;
        public bool L5;
    }

    public enum StateMode : byte
    {
        Unknown,
        Ok,
        Error
    }

    public enum FrequencyRange : byte
    {
        Range100_500 = 1,
        Range500_2500,
        Range2500_6000
    }

    public enum AntennaMode : byte
    {
        Empty,
        Log,
        OMNI
    }
    //public enum ModulationRange : byte
    //{
    //    None = 0,
    //    LCM,
    //    KFM
    //}
}
