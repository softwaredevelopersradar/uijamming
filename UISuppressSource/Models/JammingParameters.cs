﻿using System.Collections.Generic;
using GrozaSModelsDBLib;
using System.Linq;

namespace UISuppressSource
{
    using System.Collections.ObjectModel;

    public class CollectionJammingParams : ObservableCollection<JammingParameters>
    {
        public static Dictionary<string, JammingParameters> Drones { get; set; }

        public static void UpdateTypes(Dictionary<string, JammingParameters> listTypes)
        {
            Drones = listTypes;
        }

        public static JammingParameters GetType(string num)
        {
            if (Drones == null)
                return null;

            if (!Drones.Keys.Contains(num))
                return default(JammingParameters);

            return Drones[num];
        }

        //public static string GetNum(JammingParameters type)
        //{
        //    if (Drones == null)
        //        return 0;

        //    if (!Drones.Values.Contains(type))
        //        return 255;

        //    return Drones.FirstOrDefault(x => x.Value == type).Key;
        //}
    }


    public class CollectionDroneJammingParams
    {
        public Dictionary<string, JammingParameters> Drones { get; set; }
    }

    public class JammingParameters
    {
        public Modulation Modulation { get; set; }
        public int BandMHz { get; set; }
        public int ScanSpeedKHzs { get; set; }
        public byte Manipulation { get; set; }
        

        public JammingParameters()
        {
        }
    }
}
