﻿namespace UISuppressSource
{
    public class StateAntenna
    {
        public bool Log { get; set; } = false;
        public bool Omni { get; set; } = false;
    }
}
