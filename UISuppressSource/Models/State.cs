﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UISuppressSource
{
    public class State
    {
        public StateMode Synthesizer { get; set; } = StateMode.Unknown;
        public StateMode Radiation { get; set; } = StateMode.Unknown;
        public StateMode Power { get; set; } = StateMode.Unknown;
        public StateMode Error { get; set; } = StateMode.Unknown;
        public float Temperature { get; set; }
        public float Current { get; set; }
    }
}
