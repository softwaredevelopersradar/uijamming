﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using GrozaSModelsDBLib;

namespace UISuppressSource
{
    using System.Diagnostics;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class JammingView : UserControl
    {
        private JammingViewModel JammingViewModel = new JammingViewModel();

        private TableSuppressSource record2Range = new TableSuppressSource()
                                                       {
                                                           FrequencyMHz = 2442.5d,
                                                           BandMHz = 10,
                                                           Modulation = Modulation.LCM,
                                                           Deviation = 42500,
                                                           ScanSpeed = 10,
                                                           InputType = TypeInput.Manual,
                                                       };

        private TableSuppressSource record3Range = new TableSuppressSource()
                                                       {
                                                           FrequencyMHz = 5787.5d,
                                                           BandMHz = 10,
                                                           Modulation = Modulation.LCM,
                                                           Deviation = 67500,
                                                           ScanSpeed = 10,
                                                           InputType = TypeInput.Manual,
                                                       };

        #region Events
        public event EventHandler<(FrequencyRange?, TableSuppressSource)> OnChangeRecord;
        //public event EventHandler<(bool, TableSuppressGnss)> GnssStateChanged;
        //public event EventHandler<List<TableSuppressGnss>> GnssButtonsChanged;
        public event EventHandler<(bool, List<TableSuppressGnss>)> GnssStateChanged;

        public event EventHandler<TableSuppressSource> OnDoubleClickRange;
        //public event EventHandler<(FrequencyRange?, bool)> OnActivationChanged;
        public event EventHandler<List<(FrequencyRange?, bool)>> OnActivationChanged;
        public event EventHandler<bool> OnActivationSpoofChanged;
        public event EventHandler<AntennaMode> OnAntennaChanged;

        public event EventHandler OnStopAll;
        public event EventHandler OnAskStatus;
        #endregion


        #region DP

        public ExternalMode GlobalMode
        {
            get { return (ExternalMode)GetValue(GlobalModeProperty); }
            set { SetValue(GlobalModeProperty, value); }
        }

        public static readonly DependencyProperty GlobalModeProperty =
            DependencyProperty.Register("GlobalMode", typeof(ExternalMode),
            typeof(JammingView), new FrameworkPropertyMetadata(ExternalMode.Stop));


        public string DeviceName
        {
            get { return (string)GetValue(DeviceNameProperty); }
            set { SetValue(DeviceNameProperty, value); }
        }

        public static readonly DependencyProperty DeviceNameProperty =
            DependencyProperty.Register("DeviceName", typeof(string),
            typeof(JammingView), new FrameworkPropertyMetadata("AMP1"));


        public byte Test
        {
            get { return (byte)GetValue(TestProperty); }
            set { SetValue(TestProperty, value); }
        }

        public static readonly DependencyProperty TestProperty =
            DependencyProperty.Register("Test", typeof(byte),
            typeof(JammingView));


        public DllGrozaSProperties.Models.Languages CLanguage
        {
            get { return (DllGrozaSProperties.Models.Languages)GetValue(CLanguageProperty); }
            set { SetValue(CLanguageProperty, value); Translator.LoadDictionary(value); }
        }

        public static readonly DependencyProperty CLanguageProperty =
            DependencyProperty.Register("CLanguage", typeof(DllGrozaSProperties.Models.Languages),
            typeof(JammingView));

        public AntennaMode Antenna
        {
            get { return (AntennaMode)GetValue(AntennaProperty); }
            set { SetValue(AntennaProperty, value); }
        }

        public static readonly DependencyProperty AntennaProperty =
            DependencyProperty.Register("Antenna", typeof(AntennaMode),
            typeof(JammingView), new FrameworkPropertyMetadata(AntennaMode.Empty));

        public bool IsSpoofingActive
        {
            get { return (bool)GetValue(IsSpoofingActiveProperty); }
            set { SetValue(IsSpoofingActiveProperty, value); }
        }

        public static readonly DependencyProperty IsSpoofingActiveProperty =
            DependencyProperty.Register("IsSpoofingActive", typeof(bool),
            typeof(JammingView), new FrameworkPropertyMetadata(false));

        #endregion





        public JammingView()
        {
            InitializeComponent();

            this.DataContext = JammingViewModel;
            SubscribeToElementsEvents();
        }


        private void SubscribeToElementsEvents()
        {
            //Gps.StateChanged += Letters_StateChanged;
            //Glonass.StateChanged += Letters_StateChanged;
            //Beidou.StateChanged += Letters_StateChanged;
            //Galileo.StateChanged += Letters_StateChanged;

            //Glonass.SectionActivated += Gnss_SectionActivated;
            //Beidou.SectionActivated += Gnss_SectionActivated;
            //Galileo.SectionActivated += Gnss_SectionActivated;
            //Gps.SectionActivated += Gnss_SectionActivated;
            //Navigation.StateChanged += Navigation_StateChanged;
            Navigation.SectionActivated += Gnss_SectionActivated;
            //Navigation.GnssStateChanged += Navigation_RecordChanged;

            JF100_500.OnChangeRecord += JF_OnChangeRecord;
            JF500_2500.OnChangeRecord += JF_OnChangeRecord;
            JF2500_6000.OnChangeRecord += JF_OnChangeRecord;

            JF100_500.OnClearRecord += JF_OnClearRecord;
            JF500_2500.OnClearRecord += JF_OnClearRecord;
            JF2500_6000.OnClearRecord += JF_OnClearRecord;

            JF100_500.SectionActivated += JF_SectionActivated; 
            JF500_2500.SectionActivated += JF_SectionActivated;
            JF2500_6000.SectionActivated += JF_SectionActivated;

            JF100_500.OnIsWindowPropertyOpen += JF_OnIsWindowPropertyOpen; 
            JF500_2500.OnIsWindowPropertyOpen += JF_OnIsWindowPropertyOpen;
            JF2500_6000.OnIsWindowPropertyOpen += JF_OnIsWindowPropertyOpen;

            JFSpoof.SectionActivated += JFSpoof_SectionActivated;

            JAntenna.AntennaSelected += JAntenna_AntennaSelected;
        }

        private void JAntenna_AntennaSelected(object sender, AntennaMode e)
        {
            OnAntennaChanged?.Invoke(this, e);
        }

        //private void Navigation_RecordChanged(object sender, List<TableSuppressGnss> e)
        //{
        //    GnssButtonsChanged?.Invoke(this, e);
        //}

        private void JF_OnIsWindowPropertyOpen(object sender, PropertyView e)
        {
            try
            {
                e.SetLanguagePropertyGrid(CLanguage);
            }
            catch { }
        }

        //private void Navigation_StateChanged(object sender, TableSuppressGnss e)
        //{
        //    if (e.Type != TypeGNSS.Beidou)
        //        return;

        //    JF500_2500.IsEnabled = CheckLetter2Availability(e);
        //}

        //private void Gnss_SectionActivated(object sender, (bool, TableSuppressGnss) e)
        //{
        //    GnssStateChanged?.Invoke(this, e);
        //}
        private void Gnss_SectionActivated(object sender, (bool, List<TableSuppressGnss>) e)
        {
            this.Navigation.IsActivated = e.Item1;
            GnssStateChanged?.Invoke(this, e);
        }


        private void JFSpoof_SectionActivated(object sender, bool e)
        {
            
            if (Navigation.IsActivated && (this.Navigation.Gps.L1 || this.Navigation.Gps.L2))
            {
                return;
            }
            
            Navigation.IsGpsEnabled = !e;
            OnActivationSpoofChanged?.Invoke(this, e);
        }

        private void JF_SectionActivated(object sender, bool e)
        {
            FrequencyRange? range;
            switch ((sender as JammingFrequency).Name)
            {
                case "JF100_500":
                    range = FrequencyRange.Range100_500;
                    break;
                case "JF500_2500":
                    range = FrequencyRange.Range500_2500;
                    //Beidou.IsEnabled = !e;
                    Navigation.IsBeidouEnabled = !e;
                    break;
                case "JF2500_6000":
                    range = FrequencyRange.Range2500_6000;
                    break;
                default:
                    range = null;
                    break;
            }

            //OnActivationChanged?.Invoke(this, (range, e));
            OnActivationChanged?.Invoke(this, new List<(FrequencyRange?, bool)>() { (range, e) });
        }

        //private bool CheckBeidouAvailability(bool record)
        //{
        //        return false;
        //}


        #region EventHandlers

        private void JF_OnClearRecord(object sender, EventArgs e)
        {
            FrequencyRange? range;
            TableSuppressSource source;
            var control = sender as JammingFrequency;

            if (control == null) 
                return;

            switch (control.Name)
            {
                case "JF100_500":
                    range = FrequencyRange.Range100_500;
                    source = this.JammingViewModel.Range100_500;
                    break;
                case "JF500_2500":
                    range = FrequencyRange.Range500_2500;
                    source = this.JammingViewModel.Range500_2500;
                    break;
                case "JF2500_6000":
                    range = FrequencyRange.Range2500_6000;
                    source = this.JammingViewModel.Range2500_6000;
                    break;
                default:
                    range = null;
                    source = null;
                    break;
            }

            OnChangeRecord?.Invoke(this, ( range, new TableSuppressSource() { InputType = TypeInput.Manual }));

            if (control.IsActivated)
            {
                control.IsActivated = false;
                //this.OnActivationChanged?.Invoke(this, (range, false));
                OnActivationChanged?.Invoke(this, new List<(FrequencyRange?, bool)>() { (range, false) });
            }
        }



        private async void JF_OnChangeRecord(object sender, TableSuppressSource e)
        {
            var savedRecord = new TableSuppressSource();
            FrequencyRange? range;
            var control = sender as JammingFrequency;
            switch ((sender as JammingFrequency).Name)
            {
                case "JF100_500":
                    //savedRecord = JammingViewModel.Range100_500.Clone();
                    range = FrequencyRange.Range100_500;
                    if (e.FrequencyMHz < 100 || e.FrequencyMHz >= 500)
                    {
                        e.FrequencyMHz = 100;
                    }

                    break;
                case "JF500_2500":
                    //savedRecord = JammingViewModel.Range500_2500.Clone();
                    range = FrequencyRange.Range500_2500;
                    if (e.FrequencyMHz < 500 || e.FrequencyMHz >= 2500)
                    {
                        e.FrequencyMHz = 500;
                    }
                    break;
                case "JF2500_6000":
                    //savedRecord = JammingViewModel.Range2500_6000.Clone();
                    range = FrequencyRange.Range2500_6000;
                    if (e.FrequencyMHz < 2500 || e.FrequencyMHz >= 6000)
                    {
                        e.FrequencyMHz = 2500;
                    }
                    break;

                default:
                    range = null;
                    return;
                   // break;
            }


            OnChangeRecord?.Invoke(this, (range, e));

            await SwitchActivation(control, range);

            
            //if (control.IsActivated)
            //{
            //    control.IsActivated = false;
            //    this.OnActivationChanged?.Invoke(this, (range, false));
            //    await Task.Delay(500);
            //    control.IsActivated = true;
            //    this.OnActivationChanged?.Invoke(this, (range, true));
            //}
        }


        //private void Letters_StateChanged(object sender, TableSuppressGnss e)
        //{
        //    //GnssStateChanged?.Invoke(this, e);
            
        //    if (e.Type != TypeGNSS.Beidou)
        //        return;

        //    JF500_2500.IsEnabled = CheckLetter2Availability(e);
        //}

        private bool CheckLetter2Availability(TableSuppressGnss record)
        {
            if (record.L1 || record.L2)
            {
                 return false;
            }
            else return true;
        }

        


        private void JF_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var savedRecord = new TableSuppressSource();

            var range = (JammingFrequency)e.Source;
            switch (range.Name)
            {
                case "JF100_500":
                    savedRecord = JammingViewModel.Range100_500.Clone();
                    break;
                case "JF500_2500":
                    savedRecord = JammingViewModel.Range500_2500.Clone();
                    break;
                case "JF2500_6000":
                    savedRecord = JammingViewModel.Range2500_6000.Clone();
                    break;

                default:
                    break;
            }

            OnDoubleClickRange?.Invoke(this, savedRecord);
        }
        #endregion



        #region Commands

        public void SetSuppressSource(FrequencyRange range, TableSuppressSource record )
        {
            switch (range)
            {
                case FrequencyRange.Range100_500:
                    JammingViewModel.Range100_500 = record;
                    break;
                case FrequencyRange.Range500_2500:
                    JammingViewModel.Range500_2500 = record;
                    record2Range.Id = record.Id;
                    break;
                case FrequencyRange.Range2500_6000:
                    JammingViewModel.Range2500_6000 = record;
                    record3Range.Id = record.Id;
                    break;

                default:
                    break;
            }
        }


        public void SetSuppressGnss(TableSuppressGnss record)
        {
            //switch (record.Type)
            //{
            //    case TypeGNSS.Gps:
            //        JammingViewModel.RangeGps = record;
            //        break;
            //    case TypeGNSS.Glonass:
            //        JammingViewModel.RangeGlonass = record;
            //        break;
            //    case TypeGNSS.Beidou:
            //        JammingViewModel.RangeBeidou = record;
            //        break;
            //    case TypeGNSS.Galileo:
            //        JammingViewModel.RangeGalileo = record;
            //        break;

            //    default:
            //        break;
            //}
        }


        public void SetStateRange100_500(State record)
        {
           JammingViewModel.StateRange100_500 = record;
        }


        public void SetStateRange500_2500(State record)
        {
            JammingViewModel.StateRange500_2500 = record;
        }


        public void SetStateRange2500_6000(State record)
        {
            JammingViewModel.StateRange2500_6000 = record;
        }


        public void SetStateGnss(State record)
        {
            JammingViewModel.StateGNSS = record;
        }

        public void SetStateSpoof(State record)
        {
            JammingViewModel.StateSpoof = record;
        }

        public void SetIsActivatedSpoof(bool isActive)
        {
            IsSpoofingActive = isActive;
        }


        public void InitTypes(Dictionary<byte, string> types)
        {
            DroneTypes.UpdateTypes(types);
        }

        public void InitJammingParametersByTypes(Dictionary<string, JammingParameters> types)
        {
            CollectionJammingParams.UpdateTypes(types);
        }
        #endregion

        private void BtnStop_Click(object sender, RoutedEventArgs e)
        {
            UncheckedAllButtons(); //убрать?

            OnStopAll?.Invoke(this, null);
        }

        public void UncheckedAllButtons()
        {
            Navigation.IsBeidouEnabled = true;
            JF500_2500.IsEnabled = true;
            Navigation.IsActivated = false;
            //Beidou.IsEnabled = true;
            //JF500_2500.IsEnabled = true;
            //Gps.IsActivated = false;
            //Gps.btnL1.IsChecked = false;
            //Gps.btnL2.IsChecked = false;
            //Glonass.btnL1.IsChecked = false;
            //Glonass.btnL2.IsChecked = false;
            //Glonass.IsActivated = false;
            //Beidou.btnL1.IsChecked = false;
            //Beidou.btnL2.IsChecked = false;
            //Beidou.IsActivated = false;

            IsSpoofingActive = false;

            JF100_500.IsActivated = false;
            JF500_2500.IsActivated = false;
            JF2500_6000.IsActivated = false;
        }

        private void BtnStatus_Click(object sender, RoutedEventArgs e)
        {
            OnAskStatus?.Invoke(this, null);
        }
        
        private async void BtnDrone_Click(object sender, RoutedEventArgs e)
        {
            this.OnChangeRecord?.Invoke(this, (FrequencyRange.Range500_2500, record2Range));
            this.OnChangeRecord?.Invoke(this, (FrequencyRange.Range2500_6000, record3Range));

            var sw = new Stopwatch();
            sw.Start();

            while ((!this.JammingViewModel.Range500_2500.EqualTo(this.record2Range) || !this.JammingViewModel.Range2500_6000.EqualTo(this.record3Range)) && sw.Elapsed < TimeSpan.FromMilliseconds(5000))
            {
                await Task.Delay(10);
            }

            //if (!(await SwitchActivation(this.JF500_2500, FrequencyRange.Range500_2500)) && this.JammingViewModel.Range500_2500.FrequencyMHz != 0)
            //{
            //    this.JF500_2500.IsActivated = true;
            //    this.OnActivationChanged?.Invoke(this, (FrequencyRange.Range500_2500, true));
            //}

            //if (!(await SwitchActivation(this.JF2500_6000, FrequencyRange.Range2500_6000)) && this.JammingViewModel.Range2500_6000.FrequencyMHz != 0)
            //{
            //    this.JF2500_6000.IsActivated = true;
            //    this.OnActivationChanged?.Invoke(this, (FrequencyRange.Range2500_6000, true));
            //}
            if (this.JF500_2500.IsActivated || this.JF2500_6000.IsActivated)
            {
                this.JF500_2500.IsActivated = false;
                this.JF2500_6000.IsActivated = false;
                var stop = new List<(FrequencyRange?, bool)>()
                {
                    (FrequencyRange.Range500_2500, JF500_2500.IsActivated),
                    (FrequencyRange.Range2500_6000, JF2500_6000.IsActivated)
                };
                OnActivationChanged?.Invoke(this, stop);
            }

            await Task.Delay(500);

            if (this.JammingViewModel.Range500_2500.FrequencyMHz != 0)
            {
                this.JF500_2500.IsActivated = true;
            }

            if (this.JammingViewModel.Range2500_6000.FrequencyMHz != 0)
            {
                this.JF2500_6000.IsActivated = true;
            }

            var result = new List<(FrequencyRange?, bool)>()
            {
                (FrequencyRange.Range500_2500, JF500_2500.IsActivated),
                (FrequencyRange.Range2500_6000, JF2500_6000.IsActivated)
            };
            OnActivationChanged?.Invoke(this, result);
        }

        //private async Task<bool> SwitchActivation(JammingFrequency control, FrequencyRange? range)
        //{
        //    if (control.IsActivated)
        //    {
        //        control.IsActivated = false;
        //        this.OnActivationChanged?.Invoke(this, (range, false));
        //        await Task.Delay(500);
        //        control.IsActivated = true;
        //        this.OnActivationChanged?.Invoke(this, (range, true));
        //    }

        //    return control.IsActivated;
        //}

        private async Task<bool> SwitchActivation(JammingFrequency control, FrequencyRange? range)
        {
            if (control.IsActivated)
            {
                control.IsActivated = false;
                this.OnActivationChanged?.Invoke(this, new List<(FrequencyRange?, bool)>(){ (range, false)});
                await Task.Delay(500);
                control.IsActivated = true;
                this.OnActivationChanged?.Invoke(this, new List<(FrequencyRange?, bool)>() { (range, true) });
            }

            return control.IsActivated;
        }
    }
}
