﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace UISuppressSource
{
    public class DroneTypes : ObservableCollection<string>
    {
        private static Dictionary<byte, string> collection;

        public DroneTypes()
        {
            if (collection == null)
                return; 

            foreach (string rec in collection.Values)
                Add(rec);
        }
        

        public static void UpdateTypes(Dictionary<byte, string> listTypes)
        {
            collection = listTypes;
        }

        public static string GetType(byte num)
        {
            if (collection == null)
                return "-";

            if (!collection.Keys.Contains(num))
                return "-";

            return collection[num];
        }

        public static byte GetNum(string type)
        {
            if (collection == null)
                return 0;

            if (!collection.Values.Contains(type))
                return 255;

            return collection.FirstOrDefault(x => x.Value == type).Key;
        }
    }
}
