﻿using System.Windows.Input;
using System.Windows.Data;
using System.Windows;
using System.Windows.Controls;
using System.Linq;
using System.Collections.Generic;
using System;

namespace UISuppressSource
{
    public static class TextBoxEditor
    {
        #region OnTyping Int

        public static readonly DependencyProperty CommitOnIntTypingProperty = DependencyProperty.RegisterAttached("CommitOnIntTyping", typeof(bool), typeof(TextBoxEditor), new FrameworkPropertyMetadata(false, OnCommitOnIntTypingChanged));

        private static void OnCommitOnIntTypingChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var textbox = sender as TextBox;
            if (textbox == null) return;

            var wasBound = (bool)(e.OldValue);
            var needToBind = (bool)(e.NewValue);

            if (wasBound)
            {
                textbox.KeyUp -= TextBoxCommitIntValueWhileTyping;
                textbox.PreviewTextInput -= TextboxCommitPreviewTextInput;
                textbox.PreviewKeyDown -= Textbox_PreviewKeyDown;
            }

            if (needToBind)
            {
                textbox.PreviewTextInput += TextboxCommitPreviewTextInput;
                textbox.KeyUp += TextBoxCommitIntValueWhileTyping;
                textbox.PreviewKeyDown += Textbox_PreviewKeyDown;
            }
        }


        private static void TextboxCommitPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789".IndexOf(e.Text) < 0;
        }

        static void TextBoxCommitIntValueWhileTyping(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.OemMinus || e.Key == Key.Escape)
                return;

            var textbox = sender as TextBox;

            if (textbox == null) return;

            if (textbox.Text == "") return;

            BindingExpression expression = textbox.GetBindingExpression(TextBox.TextProperty);
            if (expression != null) expression.UpdateSource();
            e.Handled = true;
        }

        public static void SetCommitOnIntTyping(TextBox target, bool value)
        {
            target.SetValue(CommitOnIntTypingProperty, value);
        }

        public static bool GetCommitOnIntTyping(TextBox target)
        {
            return (bool)target.GetValue(CommitOnIntTypingProperty);
        }
        #endregion



        #region CommitOnTyping
        public static readonly DependencyProperty CommitOnTypingProperty =
            DependencyProperty.RegisterAttached("CommitOnTyping", typeof(bool), typeof(TextBoxEditor),
            new FrameworkPropertyMetadata(false, OnCommitOnTypingChanged));

        private static void OnCommitOnTypingChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var textbox = sender as TextBox;
            if (textbox == null) return;

            var wasBound = (bool)(e.OldValue);
            var needToBind = (bool)(e.NewValue);

            if (wasBound)
                textbox.KeyUp -= TextBoxCommitValueWhileTyping;

            if (needToBind)
                textbox.KeyUp += TextBoxCommitValueWhileTyping;
        }

        static void TextBoxCommitValueWhileTyping(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.OemMinus || e.Key == Key.Escape || e.Key == Key.OemPeriod || e.Key == Key.OemComma || e.Key == Key.Decimal)
                return;

            var textbox = sender as TextBox;
            if (textbox == null) return;
            BindingExpression expression = textbox.GetBindingExpression(TextBox.TextProperty);
            if (expression != null)
            {
                expression.UpdateSource();
            }
            e.Handled = true;
        }

        public static void SetCommitOnTyping(TextBox target, bool value)
        {
            target.SetValue(CommitOnTypingProperty, value);
        }

        public static bool GetCommitOnTyping(TextBox target)
        {
            return (bool)target.GetValue(CommitOnTypingProperty);
        }
        #endregion




        #region OnTyping Float

        public static readonly DependencyProperty CommitOnFloatTypingProperty = DependencyProperty.RegisterAttached("CommitOnFloatTyping", typeof(bool), typeof(TextBoxEditor), new FrameworkPropertyMetadata(false, OnCommitOnFloatTypingChanged));

        private static void OnCommitOnFloatTypingChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var textbox = sender as TextBox;
            if (textbox == null) return;

            var wasBound = (bool)(e.OldValue);
            var needToBind = (bool)(e.NewValue);

            if (wasBound)
            {
                textbox.KeyUp -= TextBoxFloatCommitValueWhileTyping;
                textbox.PreviewTextInput -= TextboxFloatCommitPreviewTextInput;
                textbox.PreviewKeyDown -= Textbox_PreviewKeyDown;
            }

            if (needToBind)
            {
                textbox.PreviewTextInput += TextboxFloatCommitPreviewTextInput;
                textbox.KeyUp += TextBoxFloatCommitValueWhileTyping;
                textbox.PreviewKeyDown += Textbox_PreviewKeyDown;
            }
        }

        static void TextBoxFloatCommitValueWhileTyping(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.OemMinus || e.Key == Key.Subtract || e.Key == Key.Escape) //e.Key==Key.OemComma||
                return;

            var textbox = sender as TextBox;

            if (textbox == null) return;

            if (textbox.Text == "") return;

            var eS = textbox.Text[textbox.Text.Length - 1];
            if (textbox.Text.Contains(",") || textbox.Text.Contains("."))
            {
                string text = textbox.Text.Remove(textbox.Text.Length - 1, 1);

                if (!(text.Contains(",") || text.Contains(".")))
                    return;

                textbox.Text = SubstituteSeparator(textbox.Text);

                textbox.SelectionStart = textbox.Text.Length;
                textbox.SelectionLength = 0;


                if (e.Key == Key.D0 || e.Key == Key.NumPad0)
                    return;
            }

            BindingExpression expression = textbox.GetBindingExpression(TextBox.TextProperty);
            if (expression != null) expression.UpdateSource();
            e.Handled = true;
        }

        private static string SubstituteSeparator(string text)
        {
            char separator = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator[0];
            string Source = text.Replace(',', separator);
            Source = Source.Replace('.', separator);
            Source = RemoveAdditionalSepparators(Source, separator);
            return Source;
        }

        private static string RemoveAdditionalSepparators(string text, char separator)
        {
            int index = text.IndexOf(separator);
            text = text.Trim(separator);
            if (!text.Contains(separator))
                text = text.Insert(index, separator.ToString());
            return text;
        }


        private static void TextboxFloatCommitPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789.,".IndexOf(e.Text) < 0;
        }


        public static void SetCommitOnFloatTyping(TextBox target, bool value)
        {
            target.SetValue(CommitOnFloatTypingProperty, value);
        }

        public static bool GetCommitOnFloatTyping(TextBox target)
        {
            return (bool)target.GetValue(CommitOnFloatTypingProperty);
        }
        #endregion

        #region CommitOnIP
        public static readonly DependencyProperty CommitOnIPProperty = DependencyProperty.RegisterAttached("CommitOnIP",
            typeof(bool), typeof(TextBoxEditor), new FrameworkPropertyMetadata(false, OnCommitOnIPChanged));

        private static void OnCommitOnIPChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var textbox = sender as TextBox;
            if (textbox == null) return;

            var wasBound = (bool)(e.OldValue);
            var needToBind = (bool)(e.NewValue);

            if (wasBound)
            {
                textbox.KeyUp -= TextBoxCommitOnIpValue;
                textbox.PreviewTextInput -= TextboxCommitOnIpPreviewInput;
            }

            if (needToBind)
            {
                textbox.PreviewTextInput += TextboxCommitOnIpPreviewInput;
                textbox.KeyUp += TextBoxCommitOnIpValue;
            }
        }

        private static void Textbox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab)
            {
                var textbox = sender as TextBox;
                var parent = textbox.Parent;
                foreach (var final in (parent as Grid).Children)
                {
                    if (final.Equals(sender)) continue;

                    if (!(final is TextBox)) continue;
                    if ((final as TextBox).IsEnabled && (final as TextBox).Focusable && !(final as TextBox).IsReadOnly)
                    {
                        (final as UIElement).Focus();
                        e.Handled = true;
                        return;
                    }
                }
                (sender as UIElement).Focus();
                e.Handled = true;
            }

        }

        private static void TextboxCommitOnIpPreviewInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789.".IndexOf(e.Text) < 0;
        }

        static void TextBoxCommitOnIpValue(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape || e.Key == Key.Tab) //e.Key == Key.Back ||
                return;

            var textbox = sender as TextBox;

            if (textbox == null) return;

            if (textbox.Text == "") return;

            BindingExpression expression = textbox.GetBindingExpression(TextBox.TextProperty);
            if (expression != null) expression.UpdateSource();
            e.Handled = true;
        }

        public static void SetCommitOnIP(TextBox target, bool value)
        {
            target.SetValue(CommitOnIPProperty, value);
        }

        public static bool GetCommitOnIP(TextBox target)
        {
            return (bool)target.GetValue(CommitOnIPProperty);
        }
        #endregion}


        #region TextBox Correct FreqMHz
        public static readonly DependencyProperty TextBoxDoubleFreqMHzChanged =
            DependencyProperty.RegisterAttached("TextBoxDoubleFreqMHz", typeof(bool), typeof(TextBoxEditor),
            new FrameworkPropertyMetadata(false, OnTextBoxDoubleFreqMHzChanged));

        private static void OnTextBoxDoubleFreqMHzChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var textBox = sender as TextBox;
            if (textBox == null) return;

            var wasBound = (bool)(e.OldValue);
            var needToBind = (bool)(e.NewValue);
            if (wasBound)
            {
                textBox.PreviewTextInput -= TextBoxDoubleFreqMHzPreviewInput;
            }
            if (needToBind)
            {
                textBox.PreviewTextInput += TextBoxDoubleFreqMHzPreviewInput;
            }
        }

        private static void TextBoxDoubleFreqMHzPreviewInput(object sender, TextCompositionEventArgs e)
        {
            try
            {
                char[] tbText = (sender as TextBox).Text.ToCharArray();

                List<string> s = new List<string>();
                for (int i = 0; i < tbText.Length; i++)
                {
                    s.Add(tbText[i].ToString());
                }

                if (e.Text == "," || e.Text == ".")
                {
                    if ((s.IndexOf(",") != -1) || s.IndexOf(".") != -1)
                    {
                        e.Handled = true;
                    }
                    // ,
                    return;
                }

                if ((Convert.ToChar(e.Text) >= '0') && (Convert.ToChar(e.Text) <= '9'))
                {
                    // цифра
                    return;
                }

                // остальные символы запрещены
                e.Handled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static void SetTextBoxDoubleFreqMHzPreviewInput(TextBox target, bool value)
        {
            target.SetValue(TextBoxDoubleFreqMHzChanged, value);
        }

        public static bool GetTextBoxDoublePreviewInput(TextBox target)
        {
            return (bool)target.GetValue(TextBoxDoubleFreqMHzChanged);
        }
        #endregion


    }
}
