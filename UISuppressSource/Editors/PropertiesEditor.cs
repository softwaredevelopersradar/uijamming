﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;
using GrozaSModelsDBLib;

namespace UISuppressSource
{
    public class PropertiesEditor : PropertyEditor
    {
        //Dictionary<string, string> dictKeyDataTemplate = new Dictionary<string, string>()
        //{
        //    { nameof(TableSuppressSource.FrequencyMHz), "FreqEditorKey" },
        //    { nameof(TableSuppressSource.BandMHz), "BandEditorKey" },
        //    { nameof(TableSuppressSource.Type), "TypeEditorKey" },
        //    { nameof(TableSuppressSource.TableSourceId), "SourceIdEditorKey" },
        //    { nameof(TableSuppressSource.Modulation), "ModulationEditorKey" },
        //    { nameof(TableSuppressSource.Deviation), "DeviationEditorKey" },
        //    { nameof(TableSuppressSource.ScanSpeed), "ScanSpeedEditorKey" },
        //    { nameof(TableSuppressSource.Manipulation), "ManipulationEditorKey" }
        //};

        //public PropertiesEditor(string PropertyName, Type DeclaringType)
        //{
        //    this.PropertyName = PropertyName;
        //    this.DeclaringType = DeclaringType;
        //    var resource = new ResourceDictionary
        //    {
        //        Source = new Uri("/UISuppressSource;component/Themes/PropertyGridEditor.xaml",
        //                UriKind.RelativeOrAbsolute)
        //    };
        //    this.InlineTemplate = resource[dictKeyDataTemplate[PropertyName]];
        //}



        ////Dictionary<string, string> dictKeyDataTemplate = new Dictionary<string, string>()
        ////{
        ////    { nameof(TableSuppressSource), "SourceJammingKey"}
        ////};

        Dictionary<Type, string> dictKeyDataTemplate = new Dictionary<Type, string>()
        {
            { typeof(TableSuppressSource), "SourceJammingKey"}
        };

        public PropertiesEditor(string PropertyName, Type DeclaringType, Type typeProperty)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/UISuppressSource;component/Themes/PropertyGridEditor.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource[dictKeyDataTemplate[typeProperty]];
        }

        public PropertiesEditor(string PropertyName, Type DeclaringType, string nameEditorKey)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/UISuppressSource;component/Themes/PropertyGridEditor.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource[nameEditorKey];
        }

        ////public PropertiesEditor(string PropertyName, Type DeclaringType)
        ////{
        ////    this.PropertyName = PropertyName;
        ////    this.DeclaringType = DeclaringType;
        ////    var resource = new ResourceDictionary
        ////    {
        ////        Source = new Uri("/UISuppressSource;component/Themes/PropertyGridEditor.xaml",
        ////                UriKind.RelativeOrAbsolute)
        ////    };
        ////    this.InlineTemplate = resource[dictKeyDataTemplate[PropertyName]];
        ////}

    }
}
