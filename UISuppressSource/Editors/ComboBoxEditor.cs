﻿
using System.Windows;
using System.Windows.Controls;

namespace UISuppressSource
{
    using System;

    using GrozaSModelsDBLib;

    using Xceed.Wpf.Toolkit;

    public static class ComboBoxEditor
    {
        #region Params
        private static string droneNameComboBoxType = string.Empty;


        public static string DroneNameComboBoxType
        {
            get { return droneNameComboBoxType; }
            set
            {
                if (droneNameComboBoxType == value)
                    return;
                droneNameComboBoxType = value;
            }
        }

        private static JammingParameters _jammingParameters;
        #endregion



        #region ComboBox Type
        public static readonly DependencyProperty ComboBoxTypeChanged = DependencyProperty.RegisterAttached("ComboBoxType", typeof(bool), typeof(ComboBoxEditor), new FrameworkPropertyMetadata(false, OnComboBoxTypeChanged));

        private static void OnComboBoxTypeChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var comboBox = sender as ComboBox;
            if (comboBox == null) return;

            var wasBound = (bool)(e.OldValue);
            var needToBind = (bool)(e.NewValue);
            if (wasBound) { comboBox.SelectionChanged -= ComboBoxType_SelectionChanged; }
            if (needToBind) { comboBox.SelectionChanged += ComboBoxType_SelectionChanged; }
        }

        public static void ComboBoxType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var parent = (sender as ComboBox).Parent;
                if (parent is Grid)
                {
                    foreach (var child in (parent as Grid).Children)
                    {
                        if (child is ComboBox)
                        {
                            switch ((child as ComboBox).Name)
                            {
                                case "cbType":
                                    DroneNameComboBoxType = (child as ComboBox).SelectedValue.ToString();
                                    _jammingParameters = CollectionJammingParams.GetType(DroneNameComboBoxType);
                                    break;
                                case "cbModulation":
                                    if (_jammingParameters != null)
                                    {
                                        switch (_jammingParameters.Modulation)
                                        {
                                            case Modulation.LCM:
                                                (child as ComboBox).SelectedIndex = 2;
                                                break;
                                            default:
                                                (child as ComboBox).SelectedIndex = (int)_jammingParameters.Modulation;
                                                break;
                                        }
                                    }
                                    break;

                                case "tbManipulation":
                                    if (_jammingParameters != null)
                                    {
                                        (child as ComboBox).SelectedValue = _jammingParameters.Manipulation;
                                    }
                                    break;
                                    
                                default:
                                    break;
                            }
                        }

                        if (child is Border)
                        {
                            if ((child as Border).Name == "tbDeviationBorder1")
                            {
                                var sUD = ((child as Border).Child as Border).Child as SingleUpDown;
                                if (_jammingParameters != null)
                                {
                                    sUD.Value = _jammingParameters.BandMHz;
                                }
                                
                            }

                            if ((child as Border).Name == "tbScSpeedBorder1")
                            {
                                var sUD = ((child as Border).Child as Border).Child as SingleUpDown;
                                if (_jammingParameters != null)
                                {
                                    sUD.Value = _jammingParameters.ScanSpeedKHzs;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        public static void SetComboBoxTypeSelected(ComboBox target, bool value)
        {
            target.SetValue(ComboBoxTypeChanged, value);
        }

        public static bool GetComboBoxTypeSelected(ComboBox target)
        {
            return (bool)target.GetValue(ComboBoxTypeChanged);
        }
        #endregion

    }
}
