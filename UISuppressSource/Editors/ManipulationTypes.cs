﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UISuppressSource
{
    public class ManipulationTypes : Collection<string>
    {
        private static Dictionary<byte, string> collection;
        public ManipulationTypes()
        {
            InitCollection();

            foreach (string rec in collection.Values)
                Add(rec);
        }


        private void InitCollection()
        {
            collection = new Dictionary<byte, string>();
            collection.Add(1, "12.5");
            collection.Add(2, "10");
            collection.Add(3, "7.5");
            collection.Add(4, "5");
            collection.Add(5, "2");
            collection.Add(6, "1");
            collection.Add(7, "0.2");
            collection.Add(8, "0.1");
            collection.Add(9, "0.05");
            collection.Add(10, "0.025");
            collection.Add(11, "0.0125");
            collection.Add(12, "0.0025");
        }

        //private void InitCollection()
        //{
        //    collection = new Dictionary<byte, string>();
        //    collection.Add(1, "0.08");
        //    collection.Add(2, "0.1");
        //    collection.Add(3, "0.133");
        //    collection.Add(4, "0.2");
        //    collection.Add(5, "0.5");
        //    collection.Add(6, "1");
        //    collection.Add(7, "5");
        //    collection.Add(8, "10");
        //    collection.Add(9, "20");
        //    collection.Add(10, "40");
        //    collection.Add(11, "80");
        //    collection.Add(12, "400");
        //}


        public static string GetType(byte num)
        {
            if (!collection.Keys.Contains(num))
                return "-";

            return collection[num];
        }

        public static byte GetNum(string type)
        {

            if (!collection.Values.Contains(type))
                return 0;

            return collection.FirstOrDefault(x => x.Value == type).Key;
        }
    }
}
