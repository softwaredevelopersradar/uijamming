﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace UISuppressSource
{
    /// <summary>
    /// Логика взаимодействия для JammingSpoofing.xaml
    /// </summary>
    public partial class JammingSpoofing : UserControl
    {
        public JammingSpoofing()
        {
            InitializeComponent();
        }


        #region Events
        public event EventHandler<bool> SectionActivated;
        #endregion



        #region DP
        public string Range
        {
            get { return (string)GetValue(RangeProperty); }
            set { SetValue(RangeProperty, value); }
        }

        public static readonly DependencyProperty RangeProperty =
            DependencyProperty.Register("Range", typeof(string),
            typeof(JammingSpoofing), new FrameworkPropertyMetadata(" "));


        public bool IsActivated
        {
            get { return (bool)GetValue(IsActivatedProperty); }
            set { SetValue(IsActivatedProperty, value); }
        }

        public static readonly DependencyProperty IsActivatedProperty =
            DependencyProperty.Register("IsActivated", typeof(bool),
            typeof(JammingSpoofing));


        public ExternalMode Mode
        {
            get { return (ExternalMode)GetValue(ModeProperty); }
            set { SetValue(ModeProperty, value); }
        }

        public static readonly DependencyProperty ModeProperty =
            DependencyProperty.Register("Mode", typeof(ExternalMode),
            typeof(JammingSpoofing), new FrameworkPropertyMetadata(ExternalMode.Stop));

        #endregion




        #region Buttons

        private void Polygon_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //IsActivated = !IsActivated;
            SectionActivated?.Invoke(this, !IsActivated);
        }

        #endregion
    }
}
