﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using GrozaSModelsDBLib;

namespace UISuppressSource
{
    /// <summary>
    /// Логика взаимодействия для PropertyView.xaml
    /// </summary>
    public partial class PropertyView : Window
    {
        public LocalProperties DroneProperties { get; private set; }
        //public TableSuppressSource General { get; set; }
        //public TableSuppressSource FreqProperties { get; private set; }

        public PropertyView(string range)
        {
            InitializeComponent();

            //General = new TableSuppressSource();
            DroneProperties = new LocalProperties() { General = new TableSuppressSource() { InputType = TypeInput.Manual } };

            //FreqProperties = ;
            //FreqProperties.PropertyChanged += PropertyChanged;
            propertyGrid.SelectedObject = DroneProperties;
            Title = range;
            Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Images/UpdateContextMenu2.png", UriKind.Absolute));
             InitEditors();
             InitProperty();
            SetFieldsVisibility();
        }


        public PropertyView(string range, TableSuppressSource recJamming)
        {
            InitializeComponent();

            recJamming.InputType = TypeInput.Manual;
            //General = recJamming.Clone();
            DroneProperties = new LocalProperties() { General = recJamming.Clone()};

            //FreqProperties.PropertyChanged += PropertyChanged;

            
            propertyGrid.SelectedObject = DroneProperties;
            Title = range;
            Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Images/UpdateContextMenu2.png", UriKind.Absolute));
            InitEditors();
            InitProperty();
            SetFieldsVisibility();
        }


        private void InitEditors()
        {
            //propertyGrid.Editors.Add(new PropertiesEditor(nameof(FreqProperties.FrequencyMHz), typeof(TableSuppressSource)));
            //propertyGrid.Editors.Add(new PropertiesEditor(nameof(FreqProperties.BandMHz), typeof(TableSuppressSource)));
            //propertyGrid.Editors.Add(new PropertiesEditor(nameof(FreqProperties.Type), typeof(TableSuppressSource)));
            //propertyGrid.Editors.Add(new PropertiesEditor(nameof(FreqProperties.TableSourceId), typeof(TableSuppressSource)));
            //propertyGrid.Editors.Add(new PropertiesEditor(nameof(FreqProperties.Modulation), typeof(TableSuppressSource)));
            //propertyGrid.Editors.Add(new PropertiesEditor(nameof(FreqProperties.Deviation), typeof(TableSuppressSource)));
            //propertyGrid.Editors.Add(new PropertiesEditor(nameof(FreqProperties.ScanSpeed), typeof(TableSuppressSource)));
            //propertyGrid.Editors.Add(new PropertiesEditor(nameof(FreqProperties.Manipulation), typeof(TableSuppressSource)));


            //Type ty = typeof(PropertyView).GetProperty("FreqProperties").DeclaringType;
            //var t = typeof(TableSuppressSource).DeclaringType;
            ////propertyGrid.Editors.Add(new PropertiesEditor(typeof(FreqProperties), typeof(PropertyView), TableSuppressSource));
            propertyGrid.Editors.Add(new PropertiesEditor(nameof(DroneProperties.General), DroneProperties.GetType(), DroneProperties.General.GetType())); 
        }


        private void PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(DroneProperties.General.Modulation):
                    SetFieldsVisibility();
                    break;
                default:
                    break;
            }

        }

        private void SetFieldsVisibility()
        {
            //if (DroneProperties.General.Modulation == Modulation.None)
            //{
            //    propertyGrid.Properties[nameof(DroneProperties.General.Deviation)].IsBrowsable = false;
            //    propertyGrid.Properties[nameof(DroneProperties.General.Manipulation)].IsBrowsable = false;
            //    propertyGrid.Properties[nameof(DroneProperties.General.ScanSpeed)].IsBrowsable = false;
            //}

            //if (DroneProperties.General.Modulation == Modulation.LCM || DroneProperties.General.Modulation == Modulation.LCM_2)
            //{
            //    propertyGrid.Properties[nameof(DroneProperties.General.Deviation)].IsBrowsable = true;
            //    propertyGrid.Properties[nameof(DroneProperties.General.Manipulation)].IsBrowsable = false;
            //    propertyGrid.Properties[nameof(DroneProperties.General.ScanSpeed)].IsBrowsable = true;
            //}

            //if (DroneProperties.General.Modulation == Modulation.KFM)
            //{
            //    propertyGrid.Properties[nameof(DroneProperties.General.Deviation)].IsBrowsable = false;
            //    propertyGrid.Properties[nameof(DroneProperties.General.Manipulation)].IsBrowsable = true;
            //    propertyGrid.Properties[nameof(DroneProperties.General.ScanSpeed)].IsBrowsable = false;
            //}
        }


        public void SetLanguagePropertyGrid(DllGrozaSProperties.Models.Languages language)
        {
            LoadTranslatorPropertyGrid(language);
            Translator.ChangeLanguagePropertyGrid(language, propertyGrid);
        }


        private void LoadTranslatorPropertyGrid(DllGrozaSProperties.Models.Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case DllGrozaSProperties.Models.Languages.EN:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/UIJamming/StringResource.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case DllGrozaSProperties.Models.Languages.RU:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/UIJamming/StringResource.RU.xaml",
                                            UriKind.Relative);
                        break;
                    case DllGrozaSProperties.Models.Languages.AZ:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/UIJamming/StringResource.AZ.xaml",
                                            UriKind.Relative);
                        break;
                    case DllGrozaSProperties.Models.Languages.SR:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/UIJamming/StringResource.SRB.xaml",
                            UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/UIJamming/StringResource.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }


        #region PropertyException

        private void InitProperty()
        {
            foreach (var property in propertyGrid.Properties)
            {
                try
                {
                    if (property.IsBrowsable == false) { continue; }

                    if (property.PropertyValue.SubProperties.Count != 0)
                    {
                        foreach (var subProperty in property.PropertyValue.SubProperties)
                            subProperty.PropertyValue.PropertyValueException += PropertyGridSubException;
                        continue;
                    }
                    property.PropertyValue.PropertyValueException += PropertyGridException;
                }
                catch (Exception ex)
                { MessageBox.Show(ex.Message); }
            }
        }
        
        private void PropertyGridSubException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.ParentValue.ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void PropertyGridException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }
        #endregion




        #region Buttons
        //private void ButtonNoApply_Click(object sender, RoutedEventArgs e)
        //{
        //    DialogResult = false;
        //}


        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            if (CorrectPropertyValues((LocalProperties)propertyGrid.SelectedObject) != null)
            {
                DialogResult = true;
            }
        }


        public LocalProperties CorrectPropertyValues(LocalProperties PropertiesWindow)
        {
            if (PropertiesWindow.General.Modulation == Modulation.LCM || PropertiesWindow.General.Modulation == Modulation.LCM_2)
            {
                CorrectValues.IsCorrectDeviation(PropertiesWindow.General);
                CorrectValues.IsCorrectScanSpeed(PropertiesWindow.General);
            }
            return PropertiesWindow;
        }


        private void gridProperty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if ((TableSuppressSource)propertyGrid.SelectedObject != null)
                {
                    DialogResult = true;
                }
            }
        }
        #endregion

    }
}
